<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="/static/form.css">
		<script type="text/javascript" src="/static/form.js"></script>
		<link rel="icon" type="image/x-icon" href="/static/favicon.ico" />
	</head>
	<body>
		<div class="header">
			<a href="https://codeberg.org"><img src="/static/codeberg.svg" height="120" alt=""></a>
			<h1>Unisciti all'associazione Codeberg e.V. e supporta attivamente lo sviluppo di software libero e open source!</h1>
		</div>
		<form id="form" method="post" action="/post">
			<div style="columns: 30em;">
				<fieldset>
					<legend>Appartenenza</legend>
					<div class="error">{{.errors.membership}}</div>
					<p class="notes">
						Come membro sostenitore sostieni direttamente le finalità di Codeberg e.V., e ricevi aggiornamenti periodici sulle attività di Codeberg e.V. Come membro attivo entri a far parte dell'associazione Codeberg e.V. con diritto di voto.
					</p>
					<div class="hbox equalwidth">
						<label><input type="radio" name="membershipType" id="supportingMember" value="supportingMember">Membro sostenitore</label>
						<label><input type="radio" name="membershipType" id="activeMember" value="activeMember">Membro attivo</label>
					</div>
					<p class="notes">
						Le persone fisiche possono iscriversi come soci sostenitori o attivi. Ler persone giuridiche
						Possono supportare Codeberg e.V. come soci sostenitori.
					</p>
					<div class="hbox equalwidth">
						<label><input type="radio" name="memberType" id="private" value="private">Persona fisica</label>
						<label><input type="radio" name="memberType" id="corporate" value="corporate">Persona giuridica</label>
					</div>
				</fieldset>
				<fieldset>
					<legend>Indirizzo</legend>
					<p class="notes">
					    Gli aggiornamenti periodici sull'attività di Codeberg e.V. e gli inviti al voto e agli incontri dei soci
					    sono inviati tramite email. Utilizziamo l'indirizzo email anche per identificarti come membro e per
					    consentirti di annullare l'iscrizione in qualunque momento scrivendo una email dall'indirizzo registrato.
					</p>
					<div class="error">{{.errors.email}}</div>
					<div class="vbox">
						<input type="text" id="email" name="email" required autocomplete="email" placeholder="E-mail">
					</div>
					<p class="notes">
						Codeberg e.V. è legalmente tenuta a mantenere un registro aggiornato degli indirizzi postali dei propri
						membri. Invieremo lettere postali solo nel caso in cui l'indirizzo email risultasse non funzionante.
					</p>
					<div class="error">{{.errors.firstName}}</div>
					<div class="error">{{.errors.name}}</div>
					<div class="error">{{.errors.addr1}}</div>
					<div class="error">{{.errors.addr2}}</div>
					<div class="error">{{.errors.zipcode}}</div>
					<div class="error">{{.errors.city}}</div>
					<div class="error">{{.errors.country}}</div>
					<div class="vbox">
						<div class="hbox">
							<input type="text" id="firstName" name="firstName" autocomplete="given-name" placeholder="Nome">
							<input type="text" id="name" name="name" autocomplete="family-name" placeholder="Cognome">
							<input type="text" id="organization" name="organization" autocomplete="organization" placeholder="Organizzazione">
						</div>
						<input type="text" id="addr1" name="addr1" required autocomplete="address-line1" placeholder="Via e numero">
						<input type="text" id="addr2" name="addr2" autocomplete="address-line2" placeholder="Seconda riga di indirizzo">
						<div class="hbox">
							<input type="text" id="zipcode" name="zipcode" size="7" required autocomplete="postal-code" placeholder="CAP">
							<input type="text" id="city" name="city" required autocomplete="city" placeholder="Comune">
							<input type="text" id="country" name="country" size="15" required autocomplete="country-name" placeholder="Paese">
						</div>
					</div>
				</fieldset>
				<fieldset>
					<legend>Supporto intangibile</legend>
					<p class="notes">
						Se vuoi contribuire al progetto in uno dei modi elencati, seleziona una o più caselle:
					</p>
					<div class="columns">
						<label><input type="checkbox" name="skillsAppDev" value="1"><span class="wrap">Sviluppo applicazioni</span></label>
						<label><input type="checkbox" name="skillsSecurity" value="1"><span class="wrap">Sicurezza informatica e penetration testing</span></label>
						<label><input type="checkbox" name="skillsDB" value="1"><span class="wrap">Replica e performance dei database</span></label>
						<label><input type="checkbox" name="skillsFS" value="1"><span class="wrap">Replica e performance di file system distribuiti</span></label>
						<label><input type="checkbox" name="skillsTax" value="1"><span class="wrap">Revisione contabile, finanza e diritto tributario</span></label>
						<label><input type="checkbox" name="skillsLegal" value="1"><span class="wrap">Legislazione informatica, diritto delle associazioni, consulenza legale pro bono</span></label>
						<label><input type="checkbox" name="skillsPR" value="1"><span class="wrap">Networking e PR, in particolare Open Source</span></label>
						<label><input type="checkbox" name="skillsFundraising" value="1"><span class="wrap">Raccolta fondi</span></label>
						<label><input type="checkbox" id="skillsOther" name="skillsOther" value="1"><input type="text" id="skills" name="skills" placeholder=""></label>
					</div>
				</fieldset>
				<fieldset>
					<legend>Dues</legend>
					<p class="notes">
						Le quote di iscrizione sono addebitate tramite addebito diretto SEPA, se possibile. Quando vorresti effettuare
						la transazione, e quanto vuoi contribuire?
					</p>
					<div class="error">{{.errors.frequency}}</div>
					<div style="columns: 10em 4;">
						<label><input type="radio" name="frequency" value="1">Ogni mese</label>
						<label><input type="radio" name="frequency" value="3">Ogni tre mesi</label>
						<label><input type="radio" name="frequency" value="6">Ogni sei mesi</label>
						<label><input type="radio" name="frequency" value="12">Ogni anno</label>
					</div>
					<p class="notes">
					    Come stabilito dal nostro statuto, puoi scegliere liberamente la somma del tuo contributo di iscrizione. La
					    nostra banca ha calcolato un importo minimo di 10 € per giustificare i costi della transazione. L'importo
					    minimo annuale per i soci attivi con diritto di voto è di 24 €.
					</p>
					<div class="error">{{.errors.contribution}}</div>
					<div style="columns: 5em 3">
						<label><input type="radio" name="contribution" value="10">10€</label>
						<label><input type="radio" name="contribution" value="15">15€</label>
						<label><input type="radio" name="contribution" value="25">25€</label>
						<label><input type="radio" name="contribution" value="50">50€</label>
						<label><input type="radio" name="contribution" value="100">100€</label>
						<label><input type="radio" name="contribution" value="150">150€</label>
						<label><input type="radio" name="contribution" value="250">250€</label>
						<label><input type="radio" name="contribution" value="custom" id="customContribution"><input type="text" name="contributionCustom" id="contributionCustom" size="5" placeholder="">€</label>
					</div>
					<p class="notes">
						Le quote di iscrizione sono addebitate tramite addebito diretto SEPA. Per favore scrivi a 
						<a href="mailto:contact@codeberg.org">contact@codeberg.org</a> per informazioni su altre modalità
						di pagamento.
					</p>
					<div class="error">{{.errors.iban}}</div>
					<div class="hbox">
						<input type="text" id="iban" name="iban" required autocomplete="off" placeholder="IBAN">
						<input type="text" id="bic" name="bic" size="11" autocomplete="off" placeholder="BIC">
					</div>
				</fieldset>
				<fieldset>
					<p style="font-size: small;">
					<!-- official SEPA DD mandate translation from https://www.europeanpaymentscouncil.eu/other/core-sdd-mandate-translations -->
					La sottoscrizione del presente mandato comporta (A) l’autorizzazione a Codeberg e.V. (DE40ZZZ00002172825) a richiedere alla banca
					del debitore l’addebito del suo conto e (B) l’autorizzazione alla banca del debitore di procedere a tale addebito
					conformemente alle disposizioni impartite da Codeberg e.V.
					Il debitore ha diritto di ottenere il rimborso dalla propria Banca secondo gli accordi ed alle condizioni che regolano
					il rapporto con quest’ultima.  Se del caso, il rimborso deve essere richiesto nel termine di 8 settimane a decorrere
					dalla data di addebito in conto.
					</p>
					<button type="submit" id="submit">Unisciti ora</button>
				</fieldset>
			</div>
		</form>
	</body>
</html>
